# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: specialization.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='specialization.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x14specialization.proto\"+\n\x0eSpecialization\x12\n\n\x02id\x18\x01 \x01(\x05\x12\r\n\x05title\x18\x02 \x01(\t\"K\n\x1aSpecializationListResponse\x12\x0e\n\x06status\x18\x01 \x01(\x08\x12\x1d\n\x04\x64\x61ta\x18\x02 \x03(\x0b\x32\x0f.Specialization\"G\n\x16SpecializationResponse\x12\x0e\n\x06status\x18\x01 \x01(\x08\x12\x1d\n\x04\x64\x61ta\x18\x02 \x01(\x0b\x32\x0f.Specialization\"#\n\x15SpecializationRequest\x12\n\n\x02id\x18\x01 \x01(\x05\x32\xb0\x01\n\x16ISpecializationService\x12L\n\x15getSpecializationList\x12\x16.SpecializationRequest\x1a\x1b.SpecializationListResponse\x12H\n\x15getSpecializationById\x12\x16.SpecializationRequest\x1a\x17.SpecializationResponseb\x06proto3'
)




_SPECIALIZATION = _descriptor.Descriptor(
  name='Specialization',
  full_name='Specialization',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='Specialization.id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='title', full_name='Specialization.title', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=24,
  serialized_end=67,
)


_SPECIALIZATIONLISTRESPONSE = _descriptor.Descriptor(
  name='SpecializationListResponse',
  full_name='SpecializationListResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='SpecializationListResponse.status', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='SpecializationListResponse.data', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=69,
  serialized_end=144,
)


_SPECIALIZATIONRESPONSE = _descriptor.Descriptor(
  name='SpecializationResponse',
  full_name='SpecializationResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='SpecializationResponse.status', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='SpecializationResponse.data', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=146,
  serialized_end=217,
)


_SPECIALIZATIONREQUEST = _descriptor.Descriptor(
  name='SpecializationRequest',
  full_name='SpecializationRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='SpecializationRequest.id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=219,
  serialized_end=254,
)

_SPECIALIZATIONLISTRESPONSE.fields_by_name['data'].message_type = _SPECIALIZATION
_SPECIALIZATIONRESPONSE.fields_by_name['data'].message_type = _SPECIALIZATION
DESCRIPTOR.message_types_by_name['Specialization'] = _SPECIALIZATION
DESCRIPTOR.message_types_by_name['SpecializationListResponse'] = _SPECIALIZATIONLISTRESPONSE
DESCRIPTOR.message_types_by_name['SpecializationResponse'] = _SPECIALIZATIONRESPONSE
DESCRIPTOR.message_types_by_name['SpecializationRequest'] = _SPECIALIZATIONREQUEST
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Specialization = _reflection.GeneratedProtocolMessageType('Specialization', (_message.Message,), {
  'DESCRIPTOR' : _SPECIALIZATION,
  '__module__' : 'specialization_pb2'
  # @@protoc_insertion_point(class_scope:Specialization)
  })
_sym_db.RegisterMessage(Specialization)

SpecializationListResponse = _reflection.GeneratedProtocolMessageType('SpecializationListResponse', (_message.Message,), {
  'DESCRIPTOR' : _SPECIALIZATIONLISTRESPONSE,
  '__module__' : 'specialization_pb2'
  # @@protoc_insertion_point(class_scope:SpecializationListResponse)
  })
_sym_db.RegisterMessage(SpecializationListResponse)

SpecializationResponse = _reflection.GeneratedProtocolMessageType('SpecializationResponse', (_message.Message,), {
  'DESCRIPTOR' : _SPECIALIZATIONRESPONSE,
  '__module__' : 'specialization_pb2'
  # @@protoc_insertion_point(class_scope:SpecializationResponse)
  })
_sym_db.RegisterMessage(SpecializationResponse)

SpecializationRequest = _reflection.GeneratedProtocolMessageType('SpecializationRequest', (_message.Message,), {
  'DESCRIPTOR' : _SPECIALIZATIONREQUEST,
  '__module__' : 'specialization_pb2'
  # @@protoc_insertion_point(class_scope:SpecializationRequest)
  })
_sym_db.RegisterMessage(SpecializationRequest)



_ISPECIALIZATIONSERVICE = _descriptor.ServiceDescriptor(
  name='ISpecializationService',
  full_name='ISpecializationService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=257,
  serialized_end=433,
  methods=[
  _descriptor.MethodDescriptor(
    name='getSpecializationList',
    full_name='ISpecializationService.getSpecializationList',
    index=0,
    containing_service=None,
    input_type=_SPECIALIZATIONREQUEST,
    output_type=_SPECIALIZATIONLISTRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='getSpecializationById',
    full_name='ISpecializationService.getSpecializationById',
    index=1,
    containing_service=None,
    input_type=_SPECIALIZATIONREQUEST,
    output_type=_SPECIALIZATIONRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ISPECIALIZATIONSERVICE)

DESCRIPTOR.services_by_name['ISpecializationService'] = _ISPECIALIZATIONSERVICE

# @@protoc_insertion_point(module_scope)
