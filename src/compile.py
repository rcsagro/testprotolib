import glob
import re

for script in glob.iglob('./rcs_protobuf_library/*.py'):
    with open(script, 'r+') as file:
        code = file.read()
        file.seek(0)
        file.write(re.sub(r'\n(import .+_pb2.*)', '\nfrom . \\1', code))
        file.truncate()